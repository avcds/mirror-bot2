class _BotCommands:
    def __init__(self):
        self.StartCommand = 'start'
        self.MirrorCommand = 'mirror'
        self.UnzipMirrorCommand = 'unzipmirror'
        self.TarMirrorCommand = 'mirrortar'
        self.CancelMirror = 'cancelmirror'
        self.CancelAllCommand = 'cancelall'
        self.ListCommand = 'list'
        self.StatusCommand = 'status'
        self.AuthorizedUsersCommand = 'users'
        self.AuthorizeCommand = 'auth'
        self.UnAuthorizeCommand = 'unauth'
        self.AddSudoCommand = 'addsudo'
        self.RmSudoCommand = 'rmsudo'
        self.PingCommand = 'ping'
        self.RestartCommand = 'restart'
        self.StatsCommand = 'stats'
        self.HelpCommand = 'help'
        self.LogCommand = 'logs'
        self.CloneCommand = "clone"
        self.WatchCommand = 'yt'
        self.TarWatchCommand = 'yttar'
        self.RepoCommand = "repo"
        self.UpdateCommand = "update"

BotCommands = _BotCommands()
